package com.management.services;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.management.contracts.PurchaseOrderDAO;
import com.management.contracts.PurchaseOrderService;
import com.management.entities.Product;
import com.management.entities.PurchaseOrder;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {
	@Autowired
	private PurchaseOrderDAO purachseOrderDAO;

	public void setPurachseOrderDAO(PurchaseOrderDAO purachseOrderDAO) {
		this.purachseOrderDAO = purachseOrderDAO;
	}
	
	@Autowired
	private SessionFactory sessionFactory;
	

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public List<PurchaseOrder> List() {
		return purachseOrderDAO.List();
	}

	@Override
	@Transactional
	public void Create(Product product, int quantity, Date formattedDateDelivery) {
		System.out.println(product +"pp");
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setDeliveryDate(formattedDateDelivery);
		purchaseOrder.setQuantity(quantity);
		purchaseOrder.setProduct(product);
		purachseOrderDAO.Create(purchaseOrder);
	}

	@Override
	@Transactional
	public PurchaseOrder GetById(int id) {
		return purachseOrderDAO.GetById(id);
	}

	@Override
	@Transactional
	public void Update(PurchaseOrder order) {
		purachseOrderDAO.Update(order);
	}

	@Override
	@Transactional
	public void  Delete(int id) {
		 purachseOrderDAO.Delete(purachseOrderDAO.GetById(id));
	}

	@Override
	@Transactional
	public List<PurchaseOrder> Recentlysupplied() {
		Session session = sessionFactory.getCurrentSession(); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>	recentlysuppliedList = session.createQuery("from PurchaseOrder ORDER BY  deliveryDate").list();

		return recentlysuppliedList;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> SearchByCode(String keySearch) {
//		Session session = this.sessionFactory.openSession();
//		session.beginTransaction();
		Session session = sessionFactory.getCurrentSession(); 
		String hql="from PurchaseOrder p where p.product.code like '%" + keySearch + "%'";
		
		Query query = session.createQuery(hql);
		return query.list();
		
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> SortBy(java.util.List<PurchaseOrder> purchaseOrder, String sortBy,
			String type) {
		Session session = this.sessionFactory.getCurrentSession();
	
		//session.beginTransaction();
		String hql = "From PurchaseOrder p  ORDER BY p." + sortBy + " " + type + "";

		Query query = session.createQuery(hql);
		List<PurchaseOrder> temporary = new LinkedList<PurchaseOrder>();
		
		temporary.addAll(query.list());
		temporary.retainAll(purchaseOrder);
		///System.out.println(temporary+"temporary");
		return temporary;
		
		
	}
}
