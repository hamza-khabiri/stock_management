package com.management.services;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.management.contracts.ProductDAO;
import com.management.contracts.ProductInventoryService;
import com.management.entities.Product;
import com.management.entities.ProductInventory;
import com.management.entities.PurchaseOrder;
import com.management.contracts.ProductService;
import com.management.contracts.PurchaseOrderService;

@Service
public class ProductServiceImpl implements ProductService, Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	private ProductDAO productDAO;
	
	@Autowired
	PurchaseOrderService purchaseOrderService;
	@Autowired
	ProductInventoryService productInventoryService;
//	@Autowired
//	PurchaseOrderService purchaseOrderService;
	@Override
	@Transactional
	public List<Product> List() {
		return productDAO.List();
	}

	@Override
	@Transactional
	public Product Update(Product product) {
		List<Product> products = productDAO.List();
//it return error page i test manual if i exist sameCode in tableProduct Same code
		for (int i = 0; i < products.size(); i++)
			if (products.get(i).equals(product))
				if (products.get(i).getId() != (product.getId()))
					return null;
		
//
//		try {
//			productDAO.updateProduct(product);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			return null;
//		}
	//	System.out.println("yes");
		productDAO.Update(product);
		return 	product;
	}

	@Override
	@Transactional
	public Product Delete(int id_product) {
	
		for (PurchaseOrder p : purchaseOrderService.List())
			if (p.getProduct().getId() == id_product)
				return null;
		for (ProductInventory p : productInventoryService.List())
			if (p.getProduct().getId() == id_product)
				return null;
	 productDAO.Delete(id_product);
	 return new Product();
	}

	@Override
	@Transactional
	public Product GetById(int id) {
		return productDAO.GetById(id);
	}

	@Override
	@Transactional
	public Product Create(String name, String code, String description, float prix) {
		Product productCreate = new Product();
		productCreate.setCode(code);
		productCreate.setName(name);
		productCreate.setDescription(description);
		productCreate.setPrice(prix);

		if (productDAO.List().contains(productCreate)) 
return null;
		 else 
			productDAO.Create(productCreate);
			return productCreate;
	}
	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Product> SortBy(List<Product> products, String sortBy, String typeOfOrder) {
		Session session = this.sessionFactory.getCurrentSession();
		
		//session.beginTransaction();
		String hql = "From Product p  ORDER BY p." + sortBy + " " + typeOfOrder + "";

		Query query = session.createQuery(hql);
		List<Product> temporary = new LinkedList<Product>();
		
		temporary.addAll(query.list());
		temporary.retainAll(products);
		return temporary;   
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Product> SearchByName(String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = "From Product p  WHERE p.name like '%" + keySearch + "%'";
		
		Query query = session.createQuery(hql);
		return query.list();

	}
}
