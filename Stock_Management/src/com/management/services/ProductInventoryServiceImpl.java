package com.management.services;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.management.contracts.ProductInventoryDAO;
import com.management.contracts.ProductInventoryService;
import com.management.entities.Product;
import com.management.entities.ProductInventory;

@Service
public class ProductInventoryServiceImpl implements ProductInventoryService {
	
	@Autowired
	private ProductInventoryDAO productInventoryDAO;
	@Autowired
	private SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public void Update(ProductInventory productInventory) {
		productInventoryDAO.Update(productInventory);
	}

	@Override
	@Transactional
	public void Delete(int id) {
		productInventoryDAO.Delete(GetById(id));
	}

	@Override
	@Transactional
	public ProductInventory GetById(int id) {    
		return productInventoryDAO.GetById(id);
	}
	@Override
	@Transactional
	public List<ProductInventory> List()  {
		
		return  productInventoryDAO.List();
		
	}
	@Override
	@Transactional
	public void Create(Product product, int quantity) {
		ProductInventory productInventoryCreate = new ProductInventory();
		productInventoryCreate.setProduct(product);
		productInventoryCreate.setQuantity(quantity);
		productInventoryDAO.Create(productInventoryCreate);
		

	}



	public ProductInventoryDAO getProductInventoryDAO() {
		return productInventoryDAO;
	}

	public void setProductInventoryDAO(ProductInventoryDAO productInventoryDAO) {
		this.productInventoryDAO = productInventoryDAO;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public java.util.List<ProductInventory> SearchByName(String keySearch) {
		Session session = sessionFactory.getCurrentSession(); 
		String hql="from ProductInventory p where p.product.name like '%" + keySearch + "%'";
		
		Query query = session.createQuery(hql);
		return query.list();
		
	}

	@Override
	@Transactional
	public java.util.List<ProductInventory> SortBy(java.util.List<ProductInventory> productInventory, String sortBy,
			String type) {
		Session session = this.sessionFactory.getCurrentSession();
		System.out.println(productInventory);
		//session.beginTransaction();
		String hql = "from ProductInventory  p  ORDER BY p." + sortBy + " " + type + "";

		Query query = session.createQuery(hql);
		List<ProductInventory> temporary = new LinkedList<ProductInventory>();
		System.out.println(temporary);
		temporary.addAll(query.list());
		temporary.retainAll(productInventory);
		///System.out.println(temporary+"temporary");
		return temporary;
	
	}
}
