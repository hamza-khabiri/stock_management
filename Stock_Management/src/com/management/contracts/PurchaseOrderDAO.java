package com.management.contracts;

import java.util.List;

import com.management.entities.PurchaseOrder;

public interface PurchaseOrderDAO {

	public void Create(PurchaseOrder p);

	public void Update(PurchaseOrder p);

	public void Delete(PurchaseOrder  p);

	public PurchaseOrder GetById(int id);

	public List<PurchaseOrder> List();

}
