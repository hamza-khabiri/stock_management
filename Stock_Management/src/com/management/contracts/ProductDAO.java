package com.management.contracts;

import java.util.List;

import com.management.entities.Product;

public interface ProductDAO {
 
	public Product Create(Product product);

	public Product Update(Product product);
 
	public Product  Delete(int id_product);

	public Product GetById(int id_product);

	public List<Product> List();
	
	
}
