package com.management.contracts;

import java.util.List;

import com.management.entities.ProductInventory;

public interface ProductInventoryDAO {

	public void Create(ProductInventory productInventory);

	public void Update(ProductInventory productInventory);

	public void Delete(ProductInventory productInventory);

	public ProductInventory GetById(int id);

	public List<ProductInventory> List();
}
