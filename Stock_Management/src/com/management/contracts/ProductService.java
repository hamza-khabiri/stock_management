package com.management.contracts;

import java.util.List;

import com.management.entities.Product;

public interface ProductService {

	public Product Create(String name, String code, String description, float prix);

	public Product Update(Product product);

	public  Product  Delete(int id_product);

	public Product GetById(int id_product);

	public List<Product> List();
	
	public List<Product> SearchByName(String key);
	
	public List<Product> SortBy(List<Product> products, String sortBy, String type);
}
