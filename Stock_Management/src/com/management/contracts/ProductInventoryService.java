package com.management.contracts;

import java.util.List;

import com.management.entities.Product;
import com.management.entities.ProductInventory;

public interface ProductInventoryService {
	public void Create(Product product, int quantity);

	public void Update(ProductInventory productInventory);

	public void Delete(int id_product_inventory);

	public ProductInventory GetById(int id_product_inventory);

	public List<ProductInventory> List();
	
	public List<ProductInventory> SortBy(List<ProductInventory> purchaseOrders, String sortBy, String type);

	java.util.List<ProductInventory> SearchByName(String keySearch);
}
