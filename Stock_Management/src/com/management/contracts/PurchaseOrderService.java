package com.management.contracts;

import java.util.Date;
import java.util.List;

import com.management.entities.Product;
import com.management.entities.PurchaseOrder;

public interface PurchaseOrderService {
	void Create(Product product, int quantity, Date formattedDateDelivery);

	public void Delete(int parseInt);

	public PurchaseOrder GetById(int parseInt);

	public void Update(PurchaseOrder newPurchaseOrderUpdate);

	public List<PurchaseOrder> Recentlysupplied();

	public List<PurchaseOrder> List();

	//public  List<PurchaseOrder> SearchByName(String keySearch);

	public List<PurchaseOrder> SearchByCode(String keySearch);
	public List<PurchaseOrder> SortBy(List<PurchaseOrder> purchaseOrder, String sortBy, String type);
}
