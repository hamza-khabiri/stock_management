package com.management.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.management.contracts.ProductService;
import com.management.entities.Product;
     
           
@ManagedBean(name="productBean")      
@SessionScoped
@Controller
public class ProductBean implements Serializable {
	//private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private static final long serialVersionUID = -3237927464866320075L;
	// for product assignment    
	private String code;
	private String name;
	private String description;
	private String price;
	private List<Product> products;

	// for update create product management
	private boolean visibleFormCreatProduct;
	private boolean visibleFormUpdateProduct;

	// for error management     
	private boolean visibleErrorDelete;
	private boolean visibleErrorSearch;

	// for sort management
	private String sortBy;
	private boolean descending;    
	   
	// for keyword search name
	private String keySearch;

	// for conserve my change temporary
	private String productNameDeleted;
	private float prix;
	private int idProductUpdate;

	@Autowired
	ProductService productService;

	@PostConstruct
	public void init() {
		this.sortBy = "name";
		this.descending = true;
		products = new ArrayList<Product>();
		products = productService.List();
	}

	public int getIdProductUpdate() {
		return idProductUpdate;
	}

	public void setIdProductUpdate(int idProductUpdate) {
		this.idProductUpdate = idProductUpdate;
	}

	public ProductBean() {}

	/**
	 * if all is valid(price and fields not all fields doesn't empty)
	 */
	public boolean formIsValide() {
		if (name.equals("") || code.equals("") || price.equals("") || description.equals("")) {
			addMessage("Field should not be empty");
			return false;
		} else {
			try {
				prix = Float.parseFloat(price);
			} catch (NumberFormatException e) {
				addMessage("Price must be a number !");
				return false;
			}
		}
		return true;
	}

	// displayFormCreatProduc because it is not visible
	public void displayFormCreatProduct() {
		System.out.println(products+"products");
		name = "";
		code = "";
		price = "";
		description = "";

		setVisibleFormCreatProduct(true);
	}

	public void createProductCancelButton() {
		setVisibleFormCreatProduct(false);
	}

	/**
	 * 
	 */
	public void createProductConfirmButton() {
		if (formIsValide()) {
			if (productService.Create(name, code, description, prix) != null) {
				updateTableOfProduct();
				setVisibleFormCreatProduct(false);
			} else
				addMessage("Code  \"" + code + "\" already use, insert another ");
		}

	}

	/**
	 * 
	 */
	public void deleteProduct() {

		try {
			int idProductDelete = Integer.parseInt(getArgummentFromJsf().get("id"));
			if (productService.Delete(idProductDelete) == null) {
				setProductNameDeleted(productService.GetById(idProductDelete).getName());
				setVisibleErrorDelete(true);
			} else
				updateTableOfProduct();
		} catch (NumberFormatException e) {
			System.out.println(" Error in deleteProduct method id : " + getArgummentFromJsf().get("id"));
		}

	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public void hideFormErrorDelete() {
		setVisibleErrorDelete(false);
	}

	/**
	 * load all contents (name , code ,.. ) of product selected for update
	 */
	public void loadProductUpdate() {
		System.out.println(getArgummentFromJsf().get("idProductUpdate"));
		try {
			idProductUpdate = Integer.parseInt(getArgummentFromJsf().get("idProductUpdate"));
			Product ProductUpdate = productService.GetById(idProductUpdate);
			code = ProductUpdate.getCode();
			name = ProductUpdate.getName();
			description = ProductUpdate.getDescription();
			price = Float.toString(ProductUpdate.getPrice());
			
			this.visibleFormUpdateProduct = true;
		} catch (NumberFormatException e) {
			System.out.println("Error in updateProduct method id : " + getArgummentFromJsf().get("id"));
		}
	}

	/**
	 * 
	 */
	public void updateProductConfirmButton() {
		if (formIsValide()) {
			Product newProductUpdate = new Product();
			newProductUpdate.setId(idProductUpdate);
			newProductUpdate.setName(name);
			newProductUpdate.setCode(code);
			newProductUpdate.setDescription(description);
			newProductUpdate.setPrice(Float.parseFloat(price));
			if (productService.Update(newProductUpdate) == null) {
				addMessage("Code  \"" + code + "\" already use ");
				return;
			}
			updateTableOfProduct();
			this.visibleFormUpdateProduct = false;

		}

	}

	/**
	 * 
	 */
	private void updateTableOfProduct() {
		
		products = new ArrayList<Product>();
		if (productService.List() != null)
			for (Product p : productService.List())
				products.add(p);

		else
			System.out.println("productService.listProduct() is null ");
	}

	public void updateProductCancelButton() {
		this.visibleFormUpdateProduct = false;
	}

	public void searchByName() {
		products = productService.SearchByName(keySearch);
		if (products.isEmpty())
			setVisibleErrorSearch(true);
	}

	public void hideFormErrorSearch() {
		products = productService.List();
		setVisibleErrorSearch(false);
	}

	public void sortProductConfirm() {
		products = productService.SortBy(products, this.sortBy, this.descending ? "DESC" : "ASC");
	}

	// for set error message diplay in xhtml file in balise h:message
	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	// get variable from jsf thanks to balise f:param
	public Map<String, String> getArgummentFromJsf() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return facesContext.getExternalContext().getRequestParameterMap();
	}

	public boolean getVisibleFormCreatProduct() {
		return visibleFormCreatProduct;
	}

	public void setVisibleFormCreatProduct(boolean visibleFormCreatProduct) {
		this.visibleFormCreatProduct = visibleFormCreatProduct;
	}

	public boolean getVisibleFormUpdateProduct() {
		return this.visibleFormUpdateProduct;
	}

	public void setVisibleFormUpdateProduct(boolean visibleFormUpdateProduct) {
		this.visibleFormUpdateProduct = visibleFormUpdateProduct;
	}

	public void setProducts(List<Product> product) {
		this.products = product;
	}

	public List<Product> getProducts() {
		return products;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}

	public boolean isVisibleErrorDelete() {
		return visibleErrorDelete;
	}

	public void setVisibleErrorDelete(boolean visibleErrorDelete) {
		this.visibleErrorDelete = visibleErrorDelete;
	}

	public boolean isVisibleErrorSearch() {
		return visibleErrorSearch;
	}

	public void setVisibleErrorSearch(boolean visibleErrorSearch) {
		this.visibleErrorSearch = visibleErrorSearch;
	}

	public String getProductNameDeleted() {
		return productNameDeleted;
	}

	public void setProductNameDeleted(String productNameDeleted) {
		this.productNameDeleted = productNameDeleted;
	}

	public boolean getDescending() {
		return this.descending;
	}
	
	public void setDescending(boolean descending) {
		this.descending = descending;
	}
	
	public String getSortBy() {
		return this.sortBy;
	}
	
	public void setSortBy(String sortBy) {
		this.sortBy= sortBy;
	}
}
