package com.management.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.management.contracts.ProductInventoryService;
import com.management.contracts.ProductService;
import com.management.entities.Product;
import com.management.entities.ProductInventory;

@Controller
@ManagedBean(name = "productInventoryBean")
@SessionScoped    
public class ProductInventoryBean {
	private String sortBy;
	private boolean descending;
	private boolean visibleCreateInventory;
	private boolean visibleUpdateInventory;
	int quantityTemp;
	private List<ProductInventory> productInventory;
	private List<Product> products;
	private String quantity;
	private String keySearch;
	private boolean visibleErrorSearch;
	private String idProductSelect;
	int id_select_temp;

	@Autowired
	ProductService productService;
	
	@Autowired
	ProductInventoryService productInventoryService;

	@PostConstruct
	public void init() {
		sortBy = "quantity";
		descending = true;
		productInventory= new ArrayList<ProductInventory>();
		products = new ArrayList<Product>();
		productInventory= productInventoryService.List();
		products = productService.List();
	}


	private void updateTable() {
		productInventory = new ArrayList<ProductInventory>();
		products = productService.List();
		productInventory = productInventoryService.List();
	}

	public void displayCreatePurchase() {
		visibleCreateInventory = true;
		products = productService.List();
		quantity = "";
	}

	public boolean formIsValide() {
		if (quantity.equals("")) {
			addMessage("fields should not empty");
			return false;
		} else {
			try {
				quantityTemp = Integer.parseInt(quantity);
				if (quantityTemp < 0) {
					addMessage("Quantite must be a  positive number ");
					return false;
				}
				return true;
			} catch (NumberFormatException e) {
				addMessage("Quantite must be a number ");
				return false;
			}
		}

	}

	public void displayCreateInventory() {
		products = productService.List();
		quantity = "";
		visibleCreateInventory = true;
	}

	public void createInventoryConfirm() {
		if (formIsValide()) {
			Product product = productService.GetById(Integer.parseInt(idProductSelect));
			productInventoryService.Create(product, quantityTemp);
			updateTable();
			visibleCreateInventory = false;
		} else {
			addMessage("Error in creat Product Inventory ");
			visibleCreateInventory = false;
		}

	}

	public void createInventoryCancel() {
		visibleCreateInventory = false;
	}

	public void deleteInventory(String id) {
		System.out.println(id);
		try {
			int idPurchaseOrder = Integer.parseInt(id);
			productInventoryService.Delete(idPurchaseOrder);
			updateTable();
		} catch (NumberFormatException e) {
			System.out.println(" Error in deleteProduct method id : " + id);
		}
	}

	public void loadInventoryUpdate(String id) {
		products = productService.List();

		try {
			id_select_temp = Integer.parseInt(id);

			ProductInventory order = productInventoryService.GetById(id_select_temp);
			quantity = Integer.toString(order.getQuantity());
			idProductSelect = order.getProduct().getId() + "";

			visibleUpdateInventory = true;
		} catch (NumberFormatException e) {
			System.out.println("Error in updateProduct method id : " + id_select_temp);
		}

	}

	public void updateInventoryConfirm() {
		if (formIsValide()) {
			ProductInventory order = productInventoryService.GetById(id_select_temp);
			Product productSelect = productService.GetById(Integer.parseInt(idProductSelect));

			order.setProduct(productSelect);
			order.setQuantity(Integer.parseInt(quantity));
			
			productInventoryService.Update(order);
			
			updateTable();
			
			visibleUpdateInventory = false;
		}

	}
	
	public void updateInventoryCancel() {
		visibleUpdateInventory = false;
	}
     
	
	public void searchByName() {
		productInventory = productInventoryService.SearchByName(keySearch);
		if (productInventory.isEmpty()) {
			visibleErrorSearch = true;
		}
	}

	public void hideFormErrorSearch() {
		visibleErrorSearch= false;
		updateTable();
	}

	public void sortPurchaseOrderConfirm() {
		productInventory  = productInventoryService.SortBy(productInventory , this.sortBy, this.descending ? "DESC" : "ASC");
	}
	
	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean isDescending() {
		return descending;
	}

	public void setDescending(boolean descending) {
		this.descending = descending;
	}

	public boolean isVisibleCreateInventory() {
		return visibleCreateInventory;
	}

	public void setVisibleCreateInventory(boolean visibleCreateInventory) {
		this.visibleCreateInventory = visibleCreateInventory;
	}

	public boolean isVisibleUpdateInventory() {
		return visibleUpdateInventory;
	}

	public void setVisibleUpdateInventory(boolean visibleUpdateInventory) {
		this.visibleUpdateInventory = visibleUpdateInventory;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public List<ProductInventory> getProductInventory() {
		return productInventory;
	}

	public void setProductInventory(List<ProductInventory> productInventory) {
		this.productInventory = productInventory;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public ProductInventoryService getProductInventoryService() {
		return productInventoryService;
	}

	public void setProductInventoryService(ProductInventoryService productInventoryService) {
		this.productInventoryService = productInventoryService;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}

	public boolean isVisibleErrorSearch() {
		return visibleErrorSearch;
	}

	public void setVisibleErrorSearch(boolean visibleErrorSearch) {
		this.visibleErrorSearch = visibleErrorSearch;
	}

	public String getIdProductSelect() {
		return idProductSelect;
	}

	public void setIdProductSelect(String idProductSelect) {
		this.idProductSelect = idProductSelect;
	}
}
