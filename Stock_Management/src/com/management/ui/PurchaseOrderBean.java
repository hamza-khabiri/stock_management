package com.management.ui;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.management.contracts.ProductService;
import com.management.contracts.PurchaseOrderService;
import com.management.entities.Product;
import com.management.entities.PurchaseOrder;

@Controller
@ManagedBean(name = "purchaseOrderBean")
@SessionScoped
public class PurchaseOrderBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String idPurchaseUpdate;        
	private Date deliveryDate;
	private String quantity;
	private List<PurchaseOrder> purchaseOrder;
	private List<Product> products;  
	private boolean visibleCreatePurchase;
	private boolean visibleUpdatePurchase;
	//private boolean  ;
	private String idProductSelect;

	private String keySearch;
	private boolean visibleErrorSearch;

	private String sortBy;
	private boolean descending;

	@Autowired
	PurchaseOrderService purchaseOrderService;
	@Autowired
	ProductService productService;

	@PostConstruct
	public void init() {
		this.sortBy = "deliveryDate";
		this.descending = true;
		System.out.println("initilisation of purchase BEAN");
		purchaseOrder = new ArrayList<PurchaseOrder>();
		products = new ArrayList<Product>();
		purchaseOrder = purchaseOrderService.List();
		products = productService.List();
	}

	public boolean formIsValide() {
		if (quantity.equals("") || deliveryDate == null) {
			System.out.println("fields should not empty");
			addMessage("fields should not empty");
			return false;
		} else {
			try {
				if (Integer.parseInt(quantity) < 0) {
					addMessage("Quantite must be a  positive number ");
					return false;
				}
				return true;
			} catch (NumberFormatException e) {
				addMessage("Quantite must be a number ");
				return false;
			}
		}
	}

	public Date getMinDate() {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DAY_OF_MONTH, 0);
		return currentDate.getTime();
	}

	public void displayCreatePurchase() {

		products = productService.List();
		deliveryDate = null;
		quantity = "";
		this.visibleCreatePurchase = true;
	}

	public void createPurchaseConfirm() {
		if (formIsValide()) {

			Product product = productService.GetById(Integer.parseInt(idProductSelect));
			purchaseOrderService.Create(product, Integer.parseInt(quantity), deliveryDate);
			updateTableOfAllPurches();
			visibleCreatePurchase = false;
		} else {
			addMessage("Error in creat Product Purchase ");
			visibleCreatePurchase = false;
		}

	}

	public void createPurchaseCancel() {
		visibleCreatePurchase = false;
	}

	public void deletePurchaseOrder(String id) {
		System.out.println(id);
		try {
			int idPurchaseOrder = Integer.parseInt(id);
			purchaseOrderService.Delete(idPurchaseOrder);

			updateTableOfAllPurches();
		} catch (NumberFormatException e) {
			System.out.println(" Error in deleteProduct method id : " + getArgummentFromJsf().get("id"));
		}
	}

	int id_select_temp;

	public void loadProductUpdate(String id) {
		products = productService.List();

		try {
			id_select_temp = Integer.parseInt(id);

			PurchaseOrder order = purchaseOrderService.GetById(id_select_temp);
			quantity = Integer.toString(order.getQuantity());
			deliveryDate = order.getDeliveryDate();
			idProductSelect = order.getProduct().getId() + "";

			this.visibleUpdatePurchase = true;
		} catch (NumberFormatException e) {
			System.out.println("Error in updateProduct method id : " + getArgummentFromJsf().get("id"));
			this.visibleUpdatePurchase = false;
		}

	}

	public void updatePurchaseConfirm() {
		if (formIsValide()) {
			PurchaseOrder order = purchaseOrderService.GetById(id_select_temp);

			Product productSelect = productService.GetById(Integer.parseInt(idProductSelect));

			order.setProduct(productSelect);
			order.setQuantity(Integer.parseInt(quantity));
			order.setDeliveryDate(deliveryDate);

			purchaseOrderService.Update(order);
			updateTableOfAllPurches();
			this.visibleUpdatePurchase = false;
		}

	}

	public void updateProductCancel() {
		setVisibleUpdatePurchase(false);
	}

	public void searchByCode() {
		purchaseOrder = purchaseOrderService.SearchByCode(keySearch);
		if (purchaseOrder.isEmpty()) {
			visibleErrorSearch = true;
			System.out.println("error");
		}
	}

		
		public void hideFormErrorSearch() {
			updateTableOfAllPurches();
			setVisibleErrorSearch(false);
		}

	public void sortPurchaseOrderConfirm() {
		purchaseOrder = purchaseOrderService.SortBy(purchaseOrder, this.sortBy, this.descending ? "DESC" : "ASC");
	}

	private void updateTableOfAllPurches() {
		purchaseOrder = new ArrayList<PurchaseOrder>();
		products = new ArrayList<Product>();
		System.out.println(purchaseOrder + " purchse Order table update table");
		purchaseOrder = purchaseOrderService.List();
		products = productService.List();
	}

	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public Map<String, String> getArgummentFromJsf() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return facesContext.getExternalContext().getRequestParameterMap();
	}

	public void displayPdf() {
//		PdfManagement pdf = new PdfManagement();
//		pdf.display();
	}

	public List<PurchaseOrder> getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(List<PurchaseOrder> purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getIdPurchaseUpdate() {
		return idPurchaseUpdate;
	}

	public void setIdPurchaseUpdate(String idPurchaseUpdate) {
		this.idPurchaseUpdate = idPurchaseUpdate;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public String getIdProductSelect() {
		return idProductSelect;
	}

	public void setIdProductSelect(String idProductSelect) {
		this.idProductSelect = idProductSelect;
	}

	public boolean isVisibleUpdatePurchase() {
		return visibleUpdatePurchase;
	}

	public void setVisibleUpdatePurchase(boolean visibleUpdatePurchase) {
		this.visibleUpdatePurchase = visibleUpdatePurchase;
	}

	public boolean isVisibleCreatePurchase() {
		return visibleCreatePurchase;
	}

	public void setVisibleCreatePurchase(boolean visibleCreatePurchase) {
		this.visibleCreatePurchase = visibleCreatePurchase;
	}

	public boolean isVisibleErrorSearch() {
		return visibleErrorSearch;
	}

	public PurchaseOrderService getPurchaseOrderService() {
		return purchaseOrderService;
	}

	public void setPurchaseOrderService(PurchaseOrderService purchaseOrderService) {
		this.purchaseOrderService = purchaseOrderService;
	}

	public void setVisibleErrorSearch(boolean visibleErrorSearch) {
		this.visibleErrorSearch = visibleErrorSearch;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean isDescending() {
		return descending;
	}

	public void setDescending(boolean descending) {
		this.descending = descending;
	}

	

}
