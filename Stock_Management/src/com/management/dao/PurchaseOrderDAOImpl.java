package com.management.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.management.contracts.PurchaseOrderDAO;
import com.management.entities.PurchaseOrder;

@Repository
public class PurchaseOrderDAOImpl implements PurchaseOrderDAO {
	@Autowired
	private SessionFactory sessionFactory;
			
	private static final long serialVersionUID = 1L;

	@Override
	public void  Create(PurchaseOrder p) {
		Session currentSession = sessionFactory.getCurrentSession();
	    currentSession.save(p);
	}

	@Override
	public void Update(PurchaseOrder purchaseOrder) {
		Session currentSession = sessionFactory.getCurrentSession();

		currentSession.update(purchaseOrder);
	}

	@Override
	public void Delete(PurchaseOrder p) {
		sessionFactory.getCurrentSession().delete(p);
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public PurchaseOrder GetById(int id) {
		return  (PurchaseOrder) sessionFactory.getCurrentSession().get(PurchaseOrder.class, id);
	}

	@Override
	public List<PurchaseOrder> List() {
		Session session = sessionFactory.getCurrentSession(); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder> PurchaseOrderList = session.createQuery("from PurchaseOrder").list();

		return PurchaseOrderList;
	}

}
