package com.management.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.management.contracts.ProductDAO;
import com.management.entities.Product;

@Repository
public class ProductDAOImpl implements ProductDAO, Serializable {
	private static final long serialVersionUID = -2317460343007715885L;

	private static final Logger logger = Logger.getLogger(ProductDAOImpl.class);

	@Autowired
	private SessionFactory session;

	@Override
	public Product Create(Product p) {
		Session session = this.session.openSession();
		Transaction tx = session.beginTransaction();
		
		try {
			session.save(p);
			tx.commit();
			
			return p;
		} catch (Exception e) {
			return null;
		}finally {
			session.close();
		}
	}


	@Override
	public Product Update(Product product) {
		Session session = this.session.openSession();
		Transaction tx = session.beginTransaction();
		
		try {
			tx = session.beginTransaction();
			session.update(product);
			tx.commit();

			return product;
		} catch (Exception e) {
			System.out.println("eee" + e);
		} finally {
			session.close();
		}
		return null;
	}

	@Override
	public Product Delete(int id) {
		Session session = this.session.openSession();
		Transaction tx = session.beginTransaction();

		Product product = GetById(id);
		
		try {
			tx = session.beginTransaction();
			session.delete(product);
		
			tx.commit();
		
			return product;
		} catch (Exception e) {
			return null;
		} finally {
			session.close();
		}

	}

	@Override  
	public Product GetById(int id) { 
		Session session = this.session.openSession();
		Transaction tx = session.beginTransaction();

		try {
			tx = session.beginTransaction();
			Product product = session.get(Product.class, id);
			tx.commit();
		
			return product;
		} catch (Exception e) {
			 logger.info("Error in getById Product: ",  e);
		} finally {
			session.close();
		}

		return null;
	}
   
	@SuppressWarnings("unchecked")  
	@Override
	public List<Product> List() {
		Session session = this.session.openSession();
		
		String hql = "FROM Product p ORDER BY p.name";
		Query query = session.createQuery(hql);
		return  query.list();
	}

	public SessionFactory getSession() {
		return this.session;
	}
	
	public void setSession(SessionFactory session) {
		this.session = session;
	}
}
