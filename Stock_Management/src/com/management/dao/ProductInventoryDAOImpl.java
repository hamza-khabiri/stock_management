package com.management.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.management.contracts.ProductInventoryDAO;
import com.management.entities.ProductInventory;

@Repository
public class ProductInventoryDAOImpl implements ProductInventoryDAO {
	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = Logger.getLogger(ProductInventoryDAOImpl.class);

	@Override
	public void Create(ProductInventory p) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(p);
	}

	@Override
	public void Update(ProductInventory product) {
		Session currentSession = sessionFactory.getCurrentSession();

		currentSession.update(product);
	}

	@Override
	public void Delete(ProductInventory product) {
		sessionFactory.getCurrentSession().delete(product);
	}

	@Override
	public ProductInventory GetById(int id) {
		return (ProductInventory) sessionFactory.getCurrentSession().get(ProductInventory.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductInventory> List() {
		Session session = sessionFactory.getCurrentSession();

		List<ProductInventory> ProductsInventoryList = session
				.createQuery("from ProductInventory  f  ORDER BY f.quantity  DESC").list();

		return ProductsInventoryList;

	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
